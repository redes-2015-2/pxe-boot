# Contribuidores  
Álvaro Fernando  
David Carlos  
Matheus Fernandes  
Maxwell Almeida  



# Servidor de Aplicações PXE

## Motivação para uso

Uma simples atividade de instalação de um sistema operacional pode se tornar um trabalhoso desafio para um administrador de redes quando o 
mesmo se depara com a execução dessa atividade em um laboratório com dezenas de máquinas. Dessa forma, o uso de um 
ambiente PXE (Preboot eXecution Environment) possibilita o boot de uma máquina a partir da rede através de uma imagem hospedada em um servidor PXE. A imagem a seguir dá uma visão geral de um ambiente PXE:  


![alt text](https://gitlab.com/redes-2015-2/pxe-boot/raw/master/images/pxe%20.png)

## Instalação
O servidor PXE utilizado possui as seguintes configurações:  
- **Sistema operacional:** CentOS 7 Minimal Installation;  
- **Endereço IP:** 192.168.1.1/24.  

Algumas aplicações são necessárias para configuração de um servidor PXE. Lista-se as seguintes: 

- **Apache HTTP server:** Servidor web, responsável pela disponbilização de imagnes via http;
- **Dhcp-server:** Responsável pela distribuição automática de ip's entre as máquinas conectadas na rede;
- **Xinetd:** Tem como função "escutar" requisições vinda de outros serviços através das portas do servidor;
- **Tftp-server:** Tem como função a transferẽncia de arquivos através do protocolo tftp;
- **Syslinux:** É um bootloader responsável pela configuração automática de instalações através de scritps e de gerenciar instalações de sistemas diferentes (Ex. debian, centos).

Executando o seguinte comando as aplicações listadas a cima serão instaladas:  

```
yum install dhcp httpd xinetd syslinux tftp-server -y

```

## Configuração do DHCP

Copie o arquivo exemplo de configuração para diretório **/etc/dhcp.**

```
cp /usr/share/doc/dhcp-4.2.5/dhcpd.conf.example /etc/dhcp/dhcpd.conf

```  

Edite o arquivo **dhcpd.conf**

```
vi /etc/dhcp/dhcpd.conf

```

Faça as modificações como mostrado a seguir.  
  
 Configure o domain name e domain-name servers:
 

```
[...]
# option definitions common to all supported networks...
option domain-name "redes.local";
option domain-name-servers server.redes.local;
[...]

```

Se esse Servidor DHCP é o oficial na rede local, deve se descomentar a seguinte linha:  

```
[...]
authoritative;
[...]

```  
Defina a subrede, a faixa de endereços ip, domain e domain name server como mostrado a seguir:  

```
[...]
subnet 192.168.1.0 netmask 255.255.255.0 {
range 192.168.1.10 192.168.1.50;
option domain-name-servers server.redes.local;
option domain-name "redes.local";
option routers 192.168.1.1;
option broadcast-address 192.168.1.255;
default-lease-time 600;
max-lease-time 7200;    
}
[...]
```  

Caso queira-se atribuir um ip fixo à uma máquina, basta inserir o ip desejado e o MAC da máquina de acordo com o padrão mostrado a seguir.  

```
[...]

host ubuntu-client {
hardware ethernet 00:22:64:4f:e9:3a; 
fixed-address 192.168.1.15; 
}

[...]
``` 

Comente outras configurações de sub-redes não utilizadas. Salve e feche o arquivo.  

Inicie o serviço do dhcp e habilite para que o mesmo seja iniciado automaticamente após o boot.  
```
systemctl enable dhcpd 

```

```
 systemctl start dhcpd

```

## Configuração do Servidor PXE

Copie os arquivos de configuração TFTP para o diretório **/var/lib/tftpboot.**

```
cd /usr/share/syslinux/
cp pxelinux.0 menu.c32 memdisk mboot.c32 chain.c32 /var/lib/tftpboot/

```

Edite o arquivo **/etc/xinetd.d/tftp**

```
vi /etc/xinetd.d/tftp

```

Habilite o servidor TFTP. Para fazer isso, modifique a opção **"disable=yes" para "no"**

```
# default: off
 # description: The tftp server serves files using the trivial file transfer \
 #       protocol.  The tftp protocol is often used to boot diskless \
 #       workstations, download configuration files to network-aware printers, \
 #       and to start the installation process for some operating systems.
 service tftp
 {
 socket_type             = dgram
 protocol                = udp
 wait                    = yes
 user                    = root
 server                  = /usr/sbin/in.tftpd
 server_args             = -s /var/lib/tftpboot
 disable                 = no
 per_source              = 11
 cps                     = 100 2
 flags                   = IPv4
}

```  

Próximo passo, crie um diretório para gravar a imagem de instalação desejada. Monte a imagem neste diretório como será mostrado a seguir. Como exemplo, considera-se que a imagem seja centos 7 minimal e esteja localizada no diretório /root.

```
mkdir /var/lib/tftpboot/centos7_x64
mount -o loop /root/CentOS-7-x86_64-Minimal-1503-01.iso /var/lib/tftpboot/centos7_x64

```

Crie um arquivo de configuração do apache para o servidor PXE no diretório **/etc/httpd/conf.d/**

```
vi /etc/httpd/conf.d/pxeboot.conf

```  

Adicione as seguintes linhas:  

```
Alias /centos6_i386 /var/lib/tftpboot/centos7_x64

<Directory /var/lib/tftpboot/centos7_x64>
Options Indexes FollowSymLinks
Order Deny,Allow
Deny from all
Allow from 127.0.0.1 192.168.1.0/24
</Directory>

```  

Salve e feche o arquivo.  

  
Em seguida, crie um diretório para configuração do servidor PXE:  

```  
mkdir /var/lib/tftpboot/pxelinux.cfg

```  

Para instalação automática com algumas configurações predefinidas, é necessário a criação de arquivos ks.cfg. A istalação feita a partir 
desses arquivos é denominada de **Instalação KickStart**. Desse modo, crie os seguintes arquivos de configuração:  
  
**Centos**
```
#version=RHEL7
# System authorization information
auth --enableshadow --passalgo=sha512

url --url="http://192.168.1.1/centos7_x64"
# Use graphical install
text
install
# Run the Setup Agent on first boot
firstboot --enable
ignoredisk --only-use=sda
# Keyboard layouts
keyboard --vckeymap=br --xlayouts='br'
# System language
lang en_US.UTF-8

# Network information
network  --bootproto=dhcp --device=enp2s0f5 --ipv6=auto --activate
network  --hostname=redes.local
# Root password
rootpw --iscrypted $6$aS2Vw8J6jJjSkZvq$Y29bj8Q/QaJA8Mf09mP4PPbO3lvcAMNE/8UmcAEWjhDh9yq0VHNMa6bRC1qc/BCZHUk3ZsPl/7wza2jAyDtGx0
# System timezone
timezone America/Sao_Paulo --isUtc --nontp
user --groups=wheel --name=redes --password=$6$UuCyPEIkBwwEBaNw$qTU8mhfv1An1Y/cWtY6j0P/aMlc0EIaBrAcDLookD3bsBmalIstdx3tMdzPLf4aOjXBRjWiCTfdmSVWaiaSE3. --iscrypted --gecos="redes"
# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=sda
autopart --type=lvm
# Partition clearing information
clearpart --all --initlabel --drives=sda

%packages
@core
kexec-tools

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

```
  
**Fedora**
```
#version=RHEL7
# System authorization information
auth --enableshadow --passalgo=sha512

url --url="http://192.168.1.1/centos7_x64"
# Use graphical install
text
install
# Run the Setup Agent on first boot
firstboot --enable
ignoredisk --only-use=sda
# Keyboard layouts
keyboard --vckeymap=br --xlayouts='br'
# System language
lang en_US.UTF-8

# Network information
network  --bootproto=dhcp --device=enp2s0f5 --ipv6=auto --activate
network  --hostname=redes.local
# Root password
rootpw --iscrypted $6$aS2Vw8J6jJjSkZvq$Y29bj8Q/QaJA8Mf09mP4PPbO3lvcAMNE/8UmcAEWjhDh9yq0VHNMa6bRC1qc/BCZHUk3ZsPl/7wza2jAyDtGx0
# System timezone
timezone America/Sao_Paulo --isUtc --nontp
user --groups=wheel --name=redes --password=$6$UuCyPEIkBwwEBaNw$qTU8mhfv1An1Y/cWtY6j0P/aMlc0EIaBrAcDLookD3bsBmalIstdx3tMdzPLf4aOjXBRjWiCTfdmSVWaiaSE3. --iscrypted --gecos="redes"
# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=sda
autopart --type=lvm
# Partition clearing information
clearpart --all --initlabel --drives=sda

%packages
@core
kexec-tools

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end


```


Agora, crie um arquivo de configuração do servidor PXE dentro do récem criado pxelinux.cfg:  

```
vi /var/lib/tftpboot/pxelinux.cfg/default

```  

Adicione as seguintes linhas:  
```
timeout 300
ONTIMEOUT 1

menu title PXE BOOT MENU REDES 2015 #soToca

label 1
menu label ^1) Install Centos 7
kernel centos7_x64/images/pxeboot/vmlinuz
append initrd=centos7_x64/images/pxeboot/initrd.img inst.ks=http://192.168.1.1/centos7_x64/ks.cfg

label 2
menu label ^2) Install Debian 8 
kernel debian8/debian-installer/amd64/linux
append initrd=debian8/debian-installer/amd64/initrd.gz inst.ks=http://192.168.1.1/debian8/ks.cfg

label 2
menu label ^3) Install Fedora 23
kernel fedora23-server/images/pxeboot/vmlinuz
append initrd=fedora23-server/images/pxeboot/initrd.img inst.ks=http://192.168.1.1/fedora23-server/ks.cfg

#label 3
#menu label ^4) Install Ubuntu 15
#kernel ubuntu15/casper/vmlinuz.efi 
#append initrd=ubuntu15/casper/initrd.lz

label 4
menu label ^5) Boot from local drive localboot

```

Note que os arquivos de configuração do Kickstart são referênciados nas linhas começadas com "append...". Salve e feche o arquivo.  
  
  
Agora é necessário configurar o DHCP para funcionar com o servidor PXE.  
  
Edite o arquivo **/etc/dhcp/dhcpd.conf**

```
vi /etc/dhcp/dhcpd.conf
```

Adicione as seguintes linhas no final do arquivo:
```
allow booting;
allow bootp;
option option-128 code 128 = string;
option option-129 code 129 = text;
next-server 192.168.1.1;
filename "pxelinux.0";
```

Salve e feche o arquivo.  
  
Para finalizar a configuração do servidor PXE, reinicie todos os serviços.  

```
service xinetd restart
service httpd restart
service dhcpd restart

```
## Configuração da Máquina cliente PXE

Para configuração da máquina cliente habilite na bios as configurações necessárias para que o boot seja efetuada pela interface de rede.


## Dificuldades
A principal dificuldade enfrentada pela equipe foi a configuração da instalação automática dos sistemas operacionais. 
Para os sistemas baseados em RedHat, foi utilizado um arquivo de kickstart, gerado na própria instalação do servidor, 
como base e, a partir dela, colocamos nossas configurações. Para os sistemas baseados em Debian, não foi encontrado como 
escrever um arquivo de kickstart e, portanto, não foi possível configurar a instalação automática para esses sistemas. 
Além disso, foram escolhidas algumas ISOs do Debian erroneamente e muito tempo foi gasto tentando configurar essas imagens.

Outra dificuldade encontrada foi com relação ao uso do vagrant/virtualbox. Muitas dos problemas consistiam na 
incompatibilidade das interfaces de redes virtuais existentes no virtualbox no uso das mesmas para boot primário. 
Dessa forma, não foi possível, como teste, a criação de máquinas virtuais como clientes PXE.

## Referências

http://www.unixmen.com/install-configure-pxe-server-client-centos-6-5/  
http://www.unixmen.com/how-to-install-dhcp-server-in-centos-and-ubuntu/  
http://www.clubedohardware.com.br/artigos/como-o-protocolo-tcp-ip-funciona-parte-2/1352  
http://www.mlaureano.org/guias_tutoriais/GuiaXinetd.php  
http://www.syslinux.org/wiki/index.php/The_Syslinux_Project
http://www.vivaolinux.com.br/artigo/Instalacao-KickStart